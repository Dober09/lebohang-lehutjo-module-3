import 'package:flutter/material.dart';

class Activities extends StatefulWidget {
  const Activities({Key? key}) : super(key: key);

  @override
  State<Activities> createState() => _ActivitiesState();
}

class _ActivitiesState extends State<Activities> {
  Widget cardOne() {
    return Card(
        elevation: 10,
        margin: const EdgeInsets.all(10),
        shadowColor: Colors.white24,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            ListTile(
              leading: Icon(Icons.work_outline_sharp),
              title: Text('Work'),
              subtitle: Text("Homework"),
              iconColor: Colors.deepOrangeAccent,
            )
          ],
        ));
  }

  Widget cardTwo() {
    return Card(
        elevation: 10,
        margin: const EdgeInsets.all(10),
        shadowColor: Colors.white24,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            ListTile(
              leading: Icon(Icons.timelapse),
              title: Text('Time'),
              subtitle: Text("Timelap"),
              iconColor: Colors.deepOrangeAccent,
            )
          ],
        ));
  }

  Widget cardThree() {
    return Card(
        elevation: 10,
        margin: const EdgeInsets.all(10),
        shadowColor: Colors.white24,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            ListTile(
              leading: Icon(Icons.volunteer_activism),
              title: Text('Help'),
              subtitle: Text("Community"),
              iconColor: Colors.deepOrangeAccent,
            )
          ],
        ));
  }

  Widget cardFour() {
    return Card(
        elevation: 10,
        margin: const EdgeInsets.all(10),
        shadowColor: Colors.white24,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            ListTile(
              leading: Icon(Icons.add_location),
              title: Text('Place'),
              subtitle: Text("Location"),
              iconColor: Colors.deepOrangeAccent,
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Features',
            style: TextStyle(
                fontFamily: 'Ubuntu',
                // color: Colors.white,
                letterSpacing: 2),
          ),
          centerTitle: true,
          elevation: 0,
        ),
        body: Column(
          children: [cardOne(), cardTwo(), cardThree(), cardFour()],
        ));
  }
}
