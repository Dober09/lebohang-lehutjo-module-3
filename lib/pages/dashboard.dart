import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  State<DashBoardPage> createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          'DashBoard',
          style:
              TextStyle(fontSize: 34, letterSpacing: 2, fontFamily: 'Ubuntu'),
        ),
      ),
      body: Container(
        color: const Color.fromARGB(223, 239, 235, 235),
        child: SafeArea(
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.symmetric(
                  vertical: 30,
                ),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        color: Colors.deepOrangeAccent,
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushReplacementNamed(context, '/home');
                          },
                          child: const Text(
                            'Home',
                            style: TextStyle(
                              fontSize: 29,
                              letterSpacing: 2,
                              fontFamily: 'Ubuntu',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/profile');
                          },
                          child: const Text(
                            'Profile',
                            style: TextStyle(
                                fontSize: 29,
                                letterSpacing: 2,
                                color: Colors.deepOrangeAccent,
                                fontFamily: 'Ubuntu'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 5,
                color: Color.fromARGB(223, 239, 235, 235),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      // color: const Color.fromARGB(174, 255, 109, 64),
                      child: TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/feature');
                        },
                        child: const Text(
                          'Features',
                          style: TextStyle(
                              fontSize: 29,
                              letterSpacing: 2,
                              color: Colors.deepOrangeAccent,
                              fontFamily: 'Ubuntu'),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      color: Colors.deepOrangeAccent,
                      child: TextButton(
                        onPressed: () {},
                        child: const Text(
                          'Results',
                          style: TextStyle(
                            fontSize: 29,
                            letterSpacing: 2,
                            fontFamily: 'Ubuntu',
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const Divider(
                height: 5,
                color: Color.fromARGB(223, 239, 235, 235),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        color: Colors.deepOrangeAccent,
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/register');
                          },
                          child: const Text(
                            'SignUp',
                            style: TextStyle(
                              fontSize: 29,
                              letterSpacing: 2,
                              fontFamily: 'Ubuntu',
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        child: TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text(
                            'SignIn',
                            style: TextStyle(
                                fontSize: 29,
                                letterSpacing: 2,
                                color: Colors.deepOrangeAccent,
                                fontFamily: 'Ubuntu'),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.deepOrangeAccent,
                width: 340,
                margin: const EdgeInsets.symmetric(vertical: 20),
                child: TextButton(
                  onPressed: () {
                    SystemNavigator.pop();
                  },
                  child: const Text(
                    'SignOut',
                    style: TextStyle(
                      fontSize: 29,
                      letterSpacing: 2,
                      fontFamily: 'Ubuntu',
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              FloatingActionButton(
                onPressed: () {
                  Navigator.pushReplacementNamed(context, '/home');
                },
                child: const Icon(Icons.home),
              )
            ],
          ),
        ),
      ),
    );
  }
}
